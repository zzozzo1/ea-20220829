# 11. version-control-system

Date: 2023-04-10

## Status

Accepted

## Context

TTA 교육 을 한다.

## Decision

버전관리 시스템을 gitlab을 쓰겠다

## Consequences

gitlab 계정을 만들고, 필요한 파일을 등록한다.