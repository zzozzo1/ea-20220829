# 7. Coding Convention Web

Date: 2021-04-02

## Status

Proposed

## Context

1. 일관된 Coding Style로 가독성, 유지보수성을 유지해야 함
2. Code Inspection 시 사소한 style 가지고 시간 소모가 없어야 함
3. 많이 사용하는 Javascript convention 중 javascript기본외 prettier, airbnb가 유명하며 이중 prettier가 가장 널리 사용됨

## Decision

JS convention은 prettier로 하며, lint를 강제한다

## Consequences

1. IDE editor 설정은 .editorconfig를 미리 만들어 배포한다
2. pre-commit hook 으로 prettier convention으로 lint가 돌도록 하여, 통과해야 commit되도록 한다
